import {createServer} from "http";

const PORT = process.env.PORT

let server = createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Server OK');
})

server.listen(PORT).once("listening", () => console.log(`Server listening on port ${PORT}`))